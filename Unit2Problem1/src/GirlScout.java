//
//
//Heather Latif
//Unit 2 Problem 1
//Girl Scout Cookie Sales
//
//Pseudocode:
//Prompt user for number of Girl Scouts
//Use array for count of how many boxes each GS sold
//Utilize array to track boxes between ranges
//Print to screen the ranges of boxes sold and count for GS in that range
//
//
//



import javax.swing.JOptionPane;
//note:this is used for a dialog box to prompt/warn a user

public class GirlScout {

	public static void main(String[] args) 
	{
		
		// Prompt for the number of girl scouts
		int size = Integer.parseInt(JOptionPane.showInputDialog("Enter the number of Girl Scouts")) ;

		int count[] = new int[5]; //array
		int i = 1;
		while(i < size +1) //+1 to include last GS
		{
			//prompt for the number of cookie boxes sold for each girl
			int cookies = Integer.parseInt(JOptionPane.showInputDialog("Enter the number of cookie boxes each Girl Scout #" + i));
			i++;
			// track the # of times a GS sold between the range
			if(cookies > 40)
			{
				count[4]++;
			}
			else if (cookies > 30)
			{
				count[3]++;
			}
			else if (cookies > 20)
			{
				count[2]++;
			}
			else if (cookies > 10)
			{
				count[1]++;
			}
			else
			{
				count[0]++;
			}
			
		}
		
		
		//Print to screen ranges and count
		System.out.println("Total Boxes\tNumber of Girl Scouts");
		System.out.println("0 to 10 \t" + count[0]);
		System.out.println("11 to 20\t" + count[1]);
		System.out.println("21 to 30\t" + count[2]);
		System.out.println("31 to 40\t" + count[3]);
		System.out.println("41 or more\t" + count[4]);
		
		

	}

}
