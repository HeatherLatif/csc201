//
//
//Heather Latif
//CSC201
//Unit1-Problem1
//Computer Configuration
//
//Pseudocode:
//1. Tell user their type of computer
//2. Display the processor, RAM, hard drive disk, audio card, 
//   video card, and speakers along with prices
//3. Display total price for all
//4. Tell user the upgrades
//5. Display upgrades for aforementioned components
//6. Display total price for upgrades
//
//


import java.util.Scanner; //for input

public class ComputerConfig {

	public static void main(String[] args) 
	{
		System.out.println("Hello, your current computer is a Alienware 14.");
		System.out.println("Your current computer has the following:");
		
		//price taken from ark.intel.com, info about Alienware was taken from dell.com 
		String processor = "Intel Core i5-4210M";
		double processorprice = 194.99;
		
		//price from oempcworld.com 
		String RAM = "8GB DDR3L at 1600MHz ";
		double RAMprice = 75.98;
		
		//price taken from newegg.com
		String harddisk = "500GB Hybrid SATA";
		double diskprice = 74.99;
		
		//no audio card given
		String audiocard = "Onboard Audio";
		double ACprice = 0.00;
		
		//price from frys.com
		String videocard = "NVIDIA GeForce GT 750M";
		double VCprice = 124.99;
		
		//price included with laptop
		String speakers = "Dolby speakers";
		double speakerprice= 0.00 ;
		
		double sum = processorprice + RAMprice + diskprice + ACprice + VCprice + speakerprice;
		
		//print format-> %-25s means align left leaving 25 spaces for string type
		System.out.printf("%-25s %-25s %-25s\n", "Component", "Component Type", "Price");
		System.out.printf("%-25s %-25s $%-25s\n", processor, "Processor", processorprice);
		System.out.printf("%-25s %-25s $%-25s\n", RAM, "RAM", RAMprice);
		System.out.printf("%-25s %-25s $%-25s\n", harddisk, "Hard Disk Drive", diskprice);
		System.out.printf("%-25s %-25s $%-25s\n", audiocard, "Audio Card", ACprice);
		System.out.printf("%-25s %-25s $%-25s\n", videocard, "Video Card", VCprice);		
		System.out.printf("%-25s %-25s $%-25s\n", speakers, "Speakers", speakerprice);
		//%.2f means print 2 places after decimal
		System.out.printf("Your computer costs: $%.2f\n", sum);
		
		System.out.println("Here are some potential upgrades for your computer and you can enter the market prices yourself.");
		
		
		String processornew = "Intel Core i7-4820k"; //334.99 @ TIGERDIRECT.COM
		String RAMnew = "Corsair Vengeance 16GB"; //147.21 @AMAZON
		String harddisknew = "Seagate Hybrid SSHD 1TB"; //109.99 @NEWEGG.COM
		String audiocardnew = "ASUS Xonar Essence STX"; //189.99 @newegg.com
		String videocardnew = "Sapphire RADEON R9 290"; //449.86 @tigerdirect.com
		String speakersnew = "Bose MusicMonitor"; //299.95 @bose.com
		
		Scanner input = new Scanner(System.in);
		
		//%s-->print format specifier for string of characters
		System.out.printf("Price for %s: ", processornew);
		
		//input.nextDouble() waits for the user to input a value
		double processornewprice = input.nextDouble();
		System.out.printf("Price for %s: ", RAMnew);
		double RAMnewprice = input.nextDouble();
		System.out.printf("Price for %s: ", harddisknew);
		double harddisknewprice = input.nextDouble();
		System.out.printf("Price for %s: ", audiocardnew);
		double audiocardnewprice = input.nextDouble();
		System.out.printf("Price for %s: ", videocardnew);
		double videocardnewprice = input.nextDouble();
		System.out.printf("Price for %s: ", speakersnew);
		double speakernewprice = input.nextDouble();
		double totalnewprice = processornewprice + RAMnewprice + harddisknewprice + audiocardnewprice + videocardnewprice + speakernewprice;
		input.close();//closes the scanner
		
		System.out.printf("\n%-25s %-25s %-25s", "Upgraded Component", "Component Type", "Price");
		System.out.printf("\n%-25s %-25s %-25.2f" , processornew, "Processor", processornewprice);
		System.out.printf("\n%-25s %-25s %-25.2f" , RAMnew, "RAM", RAMnewprice);
		System.out.printf("\n%-25s %-25s %-25.2f" , harddisknew, "Hard Disk Drive", harddisknewprice);
		System.out.printf("\n%-25s %-25s %-25.2f" , audiocardnew, "Audio Card", audiocardnewprice);
		System.out.printf("\n%-25s %-25s %-25.2f" , videocardnew, "Video Card", videocardnewprice);
		System.out.printf("\n%-25s %-25s %-25.2f\n" , speakersnew, "Speakers", speakernewprice);
		System.out.printf("The total price of your new computer is: $%,.2f.", totalnewprice);
		
		
				
		
	}

}
