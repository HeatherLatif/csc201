//
//Heather Latif	
//Unit 1 Problem 5
//Complex Numbers
//
//
//Pseudocode:
//Input first complex number
//Input second complex number
//Perform the math functions 
//Print math function 
//Print solutions to math function
//Repeat for all functions
//

import java.util.Scanner;//for input

public class ComplexNumbers 
{

	public static void main(String[] args) 
	{
		
		System.out.println("Please enter the first complex number with the real number first. ie. a + bi -> 12 13");
		Scanner input = new Scanner(System.in);
		double real1 = input.nextInt(); //reads first int
		double imag1 = input.nextInt(); //reads second int
		
		System.out.println("Please enter the second complex number with the aforementioned format");
		double real2 = input.nextInt(); //reads first int
		double imag2 = input.nextInt(); //reads second int
		
		double Raddition = (real1 + real2); //real addition
		double Iaddition = (imag1 + imag2); //imaginary addition
		//addition formula
		System.out.print("Addition: (" + real1 + " + " + real2 + ") + (" + imag1 + " + " + imag2 + ")i" );
		System.out.println(" = (" + Raddition  + ") + (" + Iaddition + ")i"); //the sum
		
		double Rsubtraction = (real1 - real2);
		double Isubtraction = (imag1 - imag2);
		//subtraction formula
		System.out.print("Subtraction: (" + real1 + " - " + real2 + ") - (" + imag1 + " - " + imag2 + ")i" );
		System.out.println(" = (" + Rsubtraction  + ") - (" + Isubtraction + ")i"); //difference
		
		double Rmultiplication = ((real1 * real2) - (imag1 * imag2));
		double Imultiplication = ((real1 * imag2) + (imag1 * real2));
		//multiplication formula
		System.out.print("Multiplication: [(" + real1 + " * " + real2 + ") - (" + imag1 + " * " + imag2 + ")]" +
							"[(" + real1 + " * " + imag2 + ") + (" + imag1 + " * " + real2 + ")]i");
		System.out.println(" = (" + Rmultiplication  + ") + (" + Imultiplication + ")i");//product
		
		double Rdivision = (((real1 * real2) + (imag1 * imag2)) / (Math.pow(real2,2) + Math.pow(imag2,2)));
		double Idivision = (((imag2 * real2) - (real1 * imag2)) / (Math.pow(real2,2) + Math.pow(imag2,2)));
		//division formula
		System.out.print("Division: [(" + real1 + " * " + real2 + ") + (" + imag1 + " * " + imag2 + ") / (" + real2 + "^2 + " + imag2 + "^2)]" + 
						"[(" + imag1 + " * " + real2 + ") - (" + real1 + " * " + imag2 + ") / (" + real2 + "^2 + " + imag2 + "^2)i]" );
		System.out.println(" = (" + Rdivision  + ") + (" + Idivision + ")i"); //quotient
		
		
		
			
		
		

	}

}
