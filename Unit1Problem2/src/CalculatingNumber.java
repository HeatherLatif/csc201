//
//
//Heather Latif
//Unit 1 Problem 2
//Calculating a Number
//
//Pseudocode:
//1. Input Student ID #
//2. Divide ID by 4, take the remainder and add 2 to get Chapter #
//3. Consider the cases 2-5 and perform addtl. division to get Exercise #
//4. Input Page # from User
//5. Output to user what programming exercise they are to perform, 
//   from what Chapter and on what page. 
//

import java.util.Scanner;

public class CalculatingNumber 
{

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter your student ID#");
		int ID = input.nextInt();
		int Chapter = ID % 4 + 2;
		System.out.println("The chapter your exercise will be in is: Chapter " + Chapter);
		
		
		int Divide = 0;
		
		switch(Chapter)
		{
		case 2:
			Divide = 26;
			break;
		case 3:
			Divide = 34;
			break;
		case 4:
			Divide = 46;
			break;
		case 5:
			Divide = 38;
			break;	
		}
		int Exercise = ID % Divide + 1;
		
		System.out.println("Your exercise number is: #" + Exercise);
		
		System.out.print("\nPlease enter the page number of your exercise: ");
		int page = input.nextInt();
		
		System.out.println("Please solve programming exercise #" + Exercise + " from chapter " + Chapter + ", on page #" + page + ".");
		
		
	
		
		
		
				
		

	}

}
