//
//Heather Latif
//CSC 201
//Unit 2 Problem 2
//Diving
//
//Pseudocode:
//7 judges 
//awarded scores are between 0-10 float point value
//highest and lowest scores are thrown out
//remaining scores are added
//multiply sum by level of difficulty
//difficulty ranges from 1.2-3.8
//total multiplied by .6 to determine diver�s score
//
//method inputValidScore
//	inputs one valid score for one judge for one diver
//	will return valid score
//method inputAllScores 
//	creates an array to store the scores for all judges for the diver
//	fill the array with a valid score from each judge
//	doesn�t take input arguments; but returns array of scores
//method inputValidDegreeOfDifficulty
//	inputs degree of difficulty of dive
//	valid degree of diff will be returned
//method calculateScore
//	calculates score for diver based on all scores from all judges and degree 	of difficulty
//main method that uses the previous methods to determine the score for the diver and then prints to the console
//
//
//

import java.util.Scanner;

public class Diving 

{
	int judges = 0;//put outside methods so other methods can use it
	
	public float inputValidScore()
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Enter judge #" + (judges+1) +" score for diver:");
		float judgescore = input.nextFloat();
		if (judgescore >= 0 && judgescore <= 10)
		{
			return judgescore;
		}
		else
		{
			System.out.println("Please enter a valid score between 0 and 10.");
			return 0;
		}
	}
	public float[] inputAllScores()
	{
		float Score[] = new float[7];
		
		while (judges != 7)
		{
		
		Score[judges] = inputValidScore();
		
		judges++;
		
		}
		return Score;
		
	}
	public float inputValidDegreeOfDifficulty()
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the difficulty of the dive for diver: ");
		float difficulty = input.nextFloat();
		if (difficulty >= 1.2 && difficulty <= 3.8)
		{
			return difficulty;
			
		}
		else
		{ 
			System.out.print("Not a valid Degree of Difficulty, Please enter valid number between 1.2 & 3.8");
			return 0;
		}
			
		
		
	}
	public float calculateScore()
	{
		float score = 0;
		float[] allScores = inputAllScores();
		
		//sort allScores
		allScores = selectionSort(allScores);
		
		for(int i=1; i< allScores.length-1; i++)
		{
			score += allScores[i];
		}
		
		score = (float) ((score * inputValidDegreeOfDifficulty()) * .6);
		
		//multiply by 0.6 and difficulty
		
		return score;
	}
	//Sorting method
	public float[] selectionSort(float[] allScores)
	{		
		int index = 0;//keeps track of the index where min # is found to use when swapping
		for (int i = 0; i < allScores.length; i++)
		{
			float min = Float.MAX_VALUE; 
			//find min
			for(int k = i+1; k < allScores.length; k++)
			{
				
				if (allScores[k] < min) //looking in the array for min # by comparing the values and then once found moving it to the front
				{
					min = allScores[k];
					index = k;
				}
					
			}
			
			//swap
			if (allScores[i] >= allScores[index])
			{
				float tempvar = allScores[i];
				allScores[i] = allScores[index];
				allScores[index] = tempvar;
				
			}
			
		}
		return allScores;
	}

	public static void main(String[] args) 
	{
		
		new Diving();
		
		
	}
	
	public Diving() //default constructor to communicate with static main method because non static methods can't communicate with other methods
	{
		float Diverscore = calculateScore();
		System.out.println("Diver's score = " + Diverscore + ".");
		
		//calculateScore();
//		float[] f = new float[] {5, 2, 4, 1, 0, 0};
//		for (int i=0; i<f.length; i++)
//			System.out.print(f[i] + " " );
//		float[] a = selectionSort(f);
//		System.out.println("");
//		for (int i=0; i<f.length; i++)
//			System.out.print(a[i] + " " );
	}

}
